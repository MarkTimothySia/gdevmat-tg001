void setup()
{
  size(1280,720,P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
}

PVector mousePos() // follows the mouse
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new PVector (x , y);
}
  
  float r_t = 20;
  float g_t = 70;
  float b_t = 255;
  float rMapped;
  float gMapped;
  float bMapped;
  float con = 0.01f;

void drawLine(PVector mouse, int size, int opp, int l, float r,float g,float b)
{
  
  mouse = mousePos();
  
  strokeWeight(size);//thickness of the stroke
  stroke(r, g, b);// color
  
  mouse.normalize();
  mouse.mult(opp * l); // length * if opp directon
  line(0, 0, mouse.x, mouse.y);
}

void draw()
{
  background(0);
  
  PVector mouse1 = new PVector();
  PVector mouse2 = new PVector();
  PVector mouse4 = new PVector();
  
  rMapped = map(noise(r_t), 0, 1, 0, 255);
  gMapped = map(noise(g_t), 0, 1, 0, 255);
  bMapped = map(noise(b_t), 0, 1, 0, 255);
  
  drawLine(mouse1, 50, 1 , 5 , rMapped,gMapped,bMapped); //Sword guards
  drawLine(mouse2, 15, 1 , 250 , rMapped, gMapped, bMapped); //Sword top
  
  drawLine(mouse4, 15, -1 , 50, 155, 155, 155); // handle
  
  r_t += con; g_t += con; b_t += con;
  //println(mouse1.mag(), mouse2.mag());
  
}
