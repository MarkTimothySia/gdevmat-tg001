void setup()
{
  size(1920, 1000, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
  
}

Ball balls[];

  int totalBalls = 10;
  float r = random(200); float g = random(150); float b = random(200); float a = random(255);
  
void draw()
{
  background(255);
  
  for(int i = 0; i < totalBalls; i++)
  {
    balls[i].movement();
    balls[i].update();
    balls[i].checkEdge();
    balls[i].render();
  }
}


void makeBalls()
{
  for(int i = 0; i < totalBalls; i++){balls[i] = new Ball(); }
}
