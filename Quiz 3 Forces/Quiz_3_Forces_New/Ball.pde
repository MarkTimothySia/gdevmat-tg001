public class Ball
{
  public PVector pos = new PVector(Window.left + 10, Window.top - 10);
  public PVector vel = new PVector(0,0);
  public PVector acc = new PVector(0,0);
  
  public float r = ceil(random(200)), g = ceil(random(200)), b = ceil(random(200)), a = 255;
  
  public int scaler = ceil(random(30 , 80));
  PVector wind = new PVector(0.1, 0);
  PVector gravity = new PVector(0, -1);
  
  void render()
  {
    //update();noStroke();
    fill(r,g,b,a);
    circle(this.pos.x, this.pos.y, this.scaler);
  }
  
  void update()
  {
    this.pos.add(this.vel);
    this.vel.add(this.acc);
    this.acc.mult(0); //resets the acceleration per Frame
  }
  
  void applyForce(PVector force)
  {
    PVector f = PVector.div(force,scaler);
    this.acc.add(f);
  }
  
  void checkEdge()
  {
    //Normal
    if(pos.x < Window.right){this.vel.x *= 1;}
    if(pos.y > Window.bottom){this.vel.y *= 1;}
    
    //Bouncer
    if(pos.x > Window.right){this.vel.x *= -1;}
    if(pos.y < Window.bottom){this.vel.y *= -1;}
  }
  
  void movement()
  {
    for(int i = 0; i < totalBalls; i++)
    {
      this.applyForce(gravity);
      this.applyForce(wind);
    }
  }
}
