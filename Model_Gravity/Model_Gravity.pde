void setup()
{
  size(1920, 1000, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.mass = 1;
  
  for (int i = 0; i < 10; i++)
  {
    movers[i] = new Mover(Window.left + offset, 300);
    movers[i].mass = i + 1;
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
    
    offset += 1;
  }
}

Mover mover;
Mover[] movers = new Mover[10];

int offset = 0;

PVector wind = new PVector(0.1f, 0);

void draw()
{
  background(255);
  
  noStroke();
  for (Mover mover : movers)
  {
    mover.applyGravity();
    mover.applyFriction();
    
    if(mover.windOn){ mover.applyForce(wind);} // Applies wind when true
    
    mover.render();
    mover.update();
    
     if (mover.position.x > Window.right) // Bounce back to the left
    {
      mover.velocity.x *= -1;
      mover.position.x = Window.right;
    }
    
    if (mover.position.y < Window.bottom) // Bounce back upward
    {
      mover.velocity.y *= -1; 
      mover.position.y = Window.bottom;
    }
    
    if(mover.position.x == Window.windowWidth) // Checks if Mover hits half screen
    {
      mover.windOn = false;
    }
  }
}
