void setup()
{
  size(1920,1000,P3D);
  camera(0, 0, (height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, 1, 0);
  
  background(0);
}
  int counter = 0;
void draw()
{
  if(counter == 1000){ clear();counter = 0;}
  
  float gaussian = randomGaussian();
  float mean = 0;//Average of the gaussian
  float standardDeviation = 200;
  float x = (standardDeviation * gaussian) + mean;
  
  float y = (random(-(height/2) , height /2 ));
  float size = random(1,50);
  
  
  noStroke();
  fill(color(ceil(random(255)), ceil(random(255)), ceil(random(255))));
  circle(x,y,size);
  
  
  counter++;
}
/*
  X = Gaussian
  Y = Random
  
  Splatters need to use random colors
  Reset everything after 1k Frames
  */
