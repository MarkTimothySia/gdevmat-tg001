class Walker
{
 float xPos;
 float yPos;
 float t_xAxis;
 float t_yAxis = 1000 ;
 Walker(){};
 Walker(float x, float y)
 {
   xPos = x;
   yPos = y;
 }
  float con = 0.01f;
  float xMapped;
  float yMapped;
  float t_size;
  float sizeMapped = 0;
  
 void render()
 {
   circle(xMapped, yMapped, sizeMapped);
 }
 
 void randomWalk()
 {
   xMapped = map(noise(t_xAxis), 0, 1, Window.left, Window.right); //range 0% to 100%, range from left to right
   yMapped = map(noise(t_yAxis),0,1, Window.bottom, Window.top);
   sizeMapped = map(noise(t_size), 0, 1, 1, 150); // makes the size from 10 - 140
   
   t_xAxis +=con;
   t_yAxis +=con;
   t_size += con;
 }
}
