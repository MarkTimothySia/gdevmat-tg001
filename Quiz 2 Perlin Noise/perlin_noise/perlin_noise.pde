void setup()
{
  size(1280,720,P3D);
  camera(0, 0, (height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, 1, 0);
  background(0);
}

  Walker walker = new Walker();
  //float t = 0; // time
  float r_t = 10;
  float g_t = 50;
  float b_t = 255;
  
  float con = 0.01f;
  float rMapped;
  float gMapped;
  float bMapped;
  
  void callOut()
  {
    rMapped = map(noise(r_t), 0, 1, 0, 255);
    gMapped = map(noise(g_t), 0, 1, 0, 255);
    bMapped = map(noise(b_t), 0, 1, 0, 255);
    noStroke();
    fill((color(rMapped,gMapped,bMapped)));
    walker.randomWalk();
    walker.render();
    r_t += con; g_t += con; b_t += con;
  }
  
void draw()
{
  callOut();
}

  //float perlin = noise(t);
  //float mapped = map(perlin,0,1,0,255);
  //t+=0.01f;
  
  
