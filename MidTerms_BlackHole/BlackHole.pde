public class BlackHole
{
  //Base from midterms Template
  
  public float scaler;
  public PVector pos;
  BlackHole(){pos = new PVector();}
  BlackHole(PVector pos){this.pos = pos;}
  
  void giveStats(float x, float y, float scaler){this.pos.x = x; this.pos.y = y;this.scaler = scaler;}
  
  public void render(){noStroke();fill(255,255,255,255); circle(pos.x, pos.y, scaler);}
}

public class Stars
{
  public PVector pos;
  public float scaler;
  public float r = random(20, 255), g = random(20, 255), b = random(20, 255), a = random(20, 255); // a is alpha (opacity)
  
  Stars(){pos = new PVector();}
  
  Stars(float xPos, float yPos){pos = new PVector(xPos, yPos);}
  
  Stars(float xPos, float yPos, float scaler){pos = new PVector(xPos, yPos); this.scaler = scaler;}
  
  Stars(PVector pos){this.pos = pos;}
  
  Stars(PVector pos, float scaler){this.pos = pos; this.scaler = scaler;}
  
  public void render(){noStroke();fill(r,g,b,a); circle(pos.x, pos.y, scaler);}
  
  public void setColor(float r, float g, float b, float a){this.r = r; this.g = g; this.b = b; this.a = a;}
}
