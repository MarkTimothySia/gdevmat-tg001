void setup()
{
  size(1920, 1000, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
  
  stars = new Stars[100]; //creates 100 stars from the start
  makeStars();
}

  int totalStars = ceil(random(10, 120));

  BlackHole blackHole = new BlackHole();
  Stars stars[]; //Initialize Stars
  
  float originX = random(Window.left, Window.right);
  float originY = random(Window.bottom, Window.top);
  float scaler = 60;
  float frame = 1;

void makeStars()
{
  for(int i = 0; i < totalStars; i++)
  {
      float genGaus = randomGaussian();
      float SD = 50;
      float meanX = random(Window.left, Window.right);
      float meanY = random(Window.bottom, Window.top);
    
      float x = SD * genGaus + meanX;
      float y = SD * genGaus + meanY;
      
      stars[i] = new Stars(x, y, random(10, 50)); //Pos x, y, Size
      stars[i].r = map(noise(random(255)), 0, 1, 0, 255);
      stars[i].g = map(noise(random(255)), 0, 1, 0, 255);
      stars[i].b = map(noise(random(255)), 0, 1, 0, 255);
  }
  blackHole.giveStats(random(Window.left, Window.right), random(Window.bottom, Window.top), random(20, 80)); //Pos x, y, size
}

void draw()
{
  background(30);
  frame++;
  
  if(frame == 100){ frame = 0; makeStars(); } // resetter
  
  for (int i = 0; i < ceil(totalStars); i++)
  {
    stars[i].render();
    PVector direction = PVector.sub(blackHole.pos, stars[i].pos); // heads towards the direction of Blackhole
    direction.normalize().mult(15);
    stars[i].pos.add(direction);
  }
  blackHole.render();
}

/*
test code
Stars star = new Stars();
Stars star1 = new Stars();
PVector correctDirection = PVector.sub(blackHole.pos,);
*/
