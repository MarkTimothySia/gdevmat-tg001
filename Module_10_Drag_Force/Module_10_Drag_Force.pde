void setup()
{
  size(1920, 1000, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  
  for (int i = 0; i < 10; i++)
  {
    movers[i] = new Mover(Window.left + offset, 300);
    movers[i].mass = i + 1;
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
    
    offset += 80;
  }
}

Mover mover;
Mover[] movers = new Mover[10];
Water water = new Water(0, -100, Window.right, Window.bottom, 0.1f);

PVector wind = new PVector(1.5f, 0);
int offset = 0;

void draw()
{
  background(255);
  
  water.render();
  noStroke();
  
  
  for (Mover mover : movers){
    mover.applyGravity();
    mover.applyFriction();

    mover.render();
    mover.update();
    
    if (mover.position.x > Window.right)
    {
      mover.velocity.x *= -1;
      mover.position.x = Window.right;
    }
    
    if (mover.position.x < Window.left)
    {
      mover.velocity.x *= 1;
      mover.position.x = Window.left;
    }
    
    if (mover.position.y < Window.bottom)
    {
      mover.velocity.y *= -1; 
      mover.position.y = Window.bottom;
    }
    
    if (water.collidedWater(mover))
    {
      mover.applyForce(water.calculateDragForce(mover)); 
    }
    else if(water.collidedWater(mover) == false){mover.applyForce(wind);}
    
  }
}
