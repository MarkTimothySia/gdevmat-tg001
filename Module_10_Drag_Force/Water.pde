class Water
{
   public float x, y;
   public float horizon, depth;
   public float cd;
   
   public Water(float x, float y, float horizon, float depth, float cd)
   {
     this.x = x;
     this.y = y;
     this.horizon = horizon; // rangeX
     this.depth = depth; //rangeY
     this.cd = cd;
   }
   
   public void render()
   {
     fill(28, 120, 186); //color of Water
     beginShape();
     vertex(x - horizon, y, 0);
     vertex(x + horizon, y, 0);
     vertex(x + horizon, y + depth, 0);
     vertex(x - horizon, y + depth, 0);
     endShape();
   }
   
   public boolean collidedWater(Mover mover)
   {
      PVector pos = mover.position; // gets the pos then returns if true
      return pos.x > this.x - this.horizon &&
          pos.x < this.x + this.horizon &&
          pos.y < this.y;
   }
   
   public PVector calculateDragForce(Mover mover)
   {
      float speed = mover.velocity.mag();
      float dragMagnitude = this.cd * speed * speed;
      
      // direction is inverse of velocity
      PVector dragForce = mover.velocity.copy();
      dragForce.mult(-1);
      
      dragForce.normalize();
      dragForce.mult(dragMagnitude);
      return dragForce;
   }
}
