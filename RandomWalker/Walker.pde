class Walker
{
 float xPos;
 float yPos;

 Walker()
 {
 };
  
 Walker(float x, float y)
 {
   xPos = x;
   yPos = y;
 }
 
 void render()
 {
   circle(xPos, yPos, 30);
 }
 
 void randomWalk()
 {
   int direction = ceil(random(8));
   
   if(direction == 1){yPos+=10;}//Up
   else if(direction == 2){yPos-=10;}//Down
   
   
   else if(direction == 3){xPos+=10;}//right
   else if(direction == 4){xPos-=10;}//left
   
   else if(direction == 5){xPos+=10; yPos+=10;}//rightUp
   else if(direction == 6){xPos+=10; yPos-=10;}//RightDwn
   else if(direction == 7){xPos-=10; yPos+=10;}//leftUP
   else if(direction == 8){xPos-=10; yPos-=10;}//leftDown
   
 }
}
