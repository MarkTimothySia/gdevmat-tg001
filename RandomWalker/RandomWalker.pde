void setup()
{
  size(1280,720,P3D);
  camera(0,0,-(height/2)/tan(PI*30/180),0,0,0,0,-1,0);
}

Walker walker = new Walker();

/*
 color blu = color(0,10,200);
 color red = color(200,10,0);
 color grn = color(20,200,0);
*/

void draw()
{
  //float picker = ceil(random(3));
  /*
  if  (picker == 1){ fill(blu);}
  else if (picker == 2){fill(red);}
  else if (picker == 3){fill(grn);}
  */
  color RNG = color(ceil(random(255)),ceil(random(255)),ceil(random(255)));
  fill(RNG);
  walker.render();
  walker.randomWalk();
  float randomNumber = ceil(random(8)); //like disregards the decimal
  println(randomNumber);
}
