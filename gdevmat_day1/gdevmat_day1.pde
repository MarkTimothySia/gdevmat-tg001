void setup() // initialization
{
  size(1980, 900, P3D); //P3D used to make things 3d
  camera(0, 0, (height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
  background(200);// makes BG
  //camera(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ)
  //eye XYZ is the position of axises
  //center XYZ is the position in the center
  //up XYZ is like a mirror 0, -1 , 1 -lets say that your parabola is opposite just use the Y to turn it upsidedown
}

void draw() // gets called each frame
{
 background(200);
 
 allDraw();
 //float y = (amp * cos(frameCount/time)* TWO_PI); // formula for making the ball go up and down
 
 osilator();
}

//color(red, green, blue) uses 255 to get the max respective RGB
//noStroke() makes the shape have no stroke
color yel = color(255, 180, 0);//makes yellow
color red = color(255, 0, 20); //makes red
color blu = color(0, 20, 255); //makes blue
color grn = color(20, 200, 10);

void allDraw()
{
 drawCartesianPlane();
 drawLinearFunction();
 drawQuadraticFunction();
 drawCircle();
 
}

void drawCartesianPlane()// makes the grid lines
{
  line(300,0,-300,0);
  line(0,300,0,-300);
  
  for( int i = -300; i <=300; i +=10)
  {
    line(i,-5,i,5);
    line(-5,i,5,i);
  }

 }
 void drawLinearFunction()//literally the diagonal line
 {
   /* 
   f(x) = x+2
   Let x be 4, then y = 6 (4,6)
   Let x be -5, then y = -3 (-5,-3)
   */
   
   for (int x = -200; x <= 200; x++)
   {
     fill (red);
    noStroke();
     circle(x, x+2, 1);
   }
}
void drawQuadraticFunction()//makes a parabola
{
  /*
  f(x) = x^2 + 2x - 5
  */
  
  for(float x = -300; x <=300; x+=0.1)
  {
    fill (blu);
    noStroke();
    circle(x * 10, (float)Math.pow(x,2) + (2 * x) - 5, 1);
  }
}

float radius = 50;
void drawCircle()//makes a circle that can be expanded or minimized
{
 for (int x = 0; x < 360; x++)
 {
   fill(yel);
    noStroke();
   circle((float)Math.cos(x) * radius, (float)Math.sin(x) * radius, 1);
 }
}

float amp = 150; // height for the bounce
float angle = 0; //based The Coding Train 3.3 Simple Harmonic Motion this is sequenced as time interval
//float angleInterval = 0.23;//????

//translate(x, y, z) moves every frame in the respective axises
float arrayCount = 24;
void osilator()
{

  fill(grn);
  for(int i = -width; i <= width; i += arrayCount)
  {
   float y = (amp * cos(angle += 0.5));//I didn't know this works XD so if i put this sa void draw all will be alligned because all have the same y variable
   circle(i,y,20);
  }
}
//Plan is that each ball is moved 2nd ball must be ahead by 0.23f
