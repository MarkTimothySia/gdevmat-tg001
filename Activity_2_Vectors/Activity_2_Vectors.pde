void setup()
{
  size(1920, 1000, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
  
  walker = new Vector();
  walker.pos.x = Window.left; 
  walker.acc = new PVector(minSpeed, 0);
}

Vector walker;

float maxSpeed = 0.1;
float minSpeed = 0.01;
float noSpeed = 0;

void draw()
{
  background(255);
  
  if(walker.pos.x <= (Window.left/2) )
  {
    walker.acc = new PVector(maxSpeed,0);
  }
  
  if(walker.pos.x >= (Window.left/2) )
  {
    walker.acc = new PVector (-maxSpeed,0);
  }
  
  if(walker.pos.x >= 0)
  {
    walker.acc = new PVector (-minSpeed,0);
    walker.pos.x = 0;
  }
  
  fill(200,0,0);
  walker.render();
}
