public class Vector
{
  public PVector pos = new PVector();
  public PVector vel = new PVector();
  public PVector acc = new PVector();
  
  public float scale = 50;
  
  void render()
  {
    update();
    circle(pos.x, pos.y, scale);
  }
 
  private void update()
  {
    this.pos.add(this.vel);
    this.vel.add(this.acc);
  }
}
